package com.gitlab.vm;

import com.gitlab.domain.Video;
import com.gitlab.domain.enumdomain.FileType;
import com.gitlab.pedrioko.core.api.impl.FilesStaticResourceLocationsImpl;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.util.ApplicationContextUtils;
import com.gitlab.pedrioko.core.view.util.StringUtil;
import com.gitlab.pedrioko.services.CrudService;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.CollectionPath;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.gitlab.pedrioko.core.view.util.ApplicationContextUtils.getBean;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VideoVM {

    private Video video;

    private String source;
    private List<Video> videos;
    private Object selectValue;

    /**
     * Inits the.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Init
    public void init() throws IOException {
        video = (Video) Executions.getCurrent().getArg().get("video");
        source = (FilesStaticResourceLocationsImpl.STATIC_FILES_PATH + (video.getShorturl().replace("\\", "/"))).replace("//", "/");
        //    source = "/file/video?id=" + video.getId();
        CrudService crudService = ApplicationContextUtils.getBean(CrudService.class);
        PathBuilder<?> videoPath = crudService.getPathBuilder(Video.class);
        Predicate where = null;
        BooleanExpression booleanExpression = videoPath.get("tipo", FileType.class).eq(FileType.MP4);
        CollectionPath collection = videoPath.getCollection("actrices", List.class);
        for (Object val : video.getActrices()) {
            where = collection.contains(val).or(where);
        }

        collection = videoPath.getCollection("categorias", List.class);
        for (Object val : video.getCategorias()) {
            where = collection.contains(val).or(where);
        }
        where = booleanExpression.and(where).and(videoPath.get("id").ne(video.getId()));
        JPAQuery<?> query = crudService.queryRand().from(videoPath)
                .where(where).orderBy(NumberExpression.random().desc()).limit(24);

   //    List<Video> tipo = (List<Video>) query.fetch();

    //    videos = tipo.stream().map(crudService::refresh).collect(Collectors.toList());
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Object getSelectValue() {
        return selectValue;
    }

    public void setSelectValue(Object selectValue) {
        this.selectValue = selectValue;
    }

    @Command
    public void actionOnDoubleClick(@BindingParam("action") String action, @BindingParam("value") Object selectValue) {
        Action bean = (Action) getBean(StringUtil.getDescapitalize(action));
        CrudActionEvent event = new CrudActionEvent();
        event.setValue(this.selectValue);
        event.setFormstate(bean.getFormState());
        bean.actionPerform(event);
    }
}
