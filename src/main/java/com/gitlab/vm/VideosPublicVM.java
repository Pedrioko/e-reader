package com.gitlab.vm;


import com.gitlab.domain.Actriz;
import com.gitlab.domain.Categoria;
import com.gitlab.pedrioko.core.view.navegation.NavegationFilters;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.viewers.crud.CrudView;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.CheckEvent;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;

import java.util.LinkedHashSet;
import java.util.Set;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VideosPublicVM extends GenericForwardComposer {

    @Wire
    private
    CrudView crudView;
    @Wire
    private
    NavegationFilters navegationFilters;

    @Override
    public void doAfterCompose(Component c) throws Exception {
        super.doAfterCompose(c);
      //  crudView.onlyEnable(Arrays.asList());
        crudView.setPageSize(30);
        Set<Categoria> categorias = new LinkedHashSet<>();
        Set<Actriz> actrices = new LinkedHashSet<>();
        navegationFilters.addEventListener(Events.ON_CHECK, (e) -> {
            CheckEvent checkEvent = (CheckEvent) e;
            Object value = ReflectionZKUtil.getValue(checkEvent.getTarget());
            if (value instanceof Categoria) {
                if (checkEvent.isChecked()) categorias.add((Categoria) value);
                else categorias.remove((Categoria) value);
                crudView.addRootParams("categorias", categorias);
            }
            if (value instanceof Actriz) {
                if (checkEvent.isChecked()) actrices.add((Actriz) value);
                else actrices.remove((Actriz) value);
                crudView.addRootParams("actrices", actrices);
            }
            crudView.update();
            System.out.println(e);
        });
    }
}
