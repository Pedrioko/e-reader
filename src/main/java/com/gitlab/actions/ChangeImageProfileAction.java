package com.gitlab.actions;

import com.gitlab.domain.Actriz;
import com.gitlab.pedrioko.core.lang.FileEntity;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.view.enums.MessageType;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.util.ZKUtil;
import com.gitlab.pedrioko.core.zk.component.upload.FileUpload;
import com.gitlab.pedrioko.services.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.zkoss.util.media.Media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ToolAction
@Order(0)
public class ChangeImageProfileAction implements Action {

    @Autowired
    private CrudService crudService;
    private Media[] media;

    @Override
    public String getIcon() {
        return "fas fa-user-circle";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {

        Actriz value = (Actriz) event.getValue();
        if (value == null) {
            ZKUtil.showMessage(ReflectionZKUtil.getLabel("seleccione"), MessageType.WARNING);
        } else {

            List<FileEntity> values = new ArrayList<>();

            FileUpload.get(1, values, () -> {
                if (!values.isEmpty()) {
                    value.setPicture(values.get(0));
                    crudService.save(value);
                    if ( event.getCrudViewParent() != null) event.getCrudViewParent().update();

                }
            });

        }
    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(Actriz.class);
    }

    @Override
    public String getLabel() {
        return "Change image profile";
    }

    @Override
    public String getClasses() {
        return "btn";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 0;
    }

    @Override
    public String getColor() {
        return "#D980FA";
    }

    @Override
    public int getGroup() {
        return 2;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("Change image profile");
    }

    @Override
    public boolean isDefault() {
        return true;
    }

    @Override
    public boolean MenuSupported() {
        return true;
    }
}
