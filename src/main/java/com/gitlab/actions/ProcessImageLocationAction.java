package com.gitlab.actions;

import com.gitlab.domain.Image;
import com.gitlab.domain.SourcesLocation;
import com.gitlab.domain.enumdomain.FileType;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.view.enums.MessageType;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.util.ZKUtil;
import com.gitlab.pedrioko.services.CrudService;
import com.gitlab.pedrioko.services.ThreadService;
import com.gitlab.pedrioko.services.VideoService;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@ToolAction
@Order(0)
public class ProcessImageLocationAction implements Action {

    @Autowired
    private CrudService crudService;
    @Autowired
    private VideoService videoService;
    @Autowired
    private ThreadService taskExecutor;

    @Override
    public String getIcon() {
        return "fas fa-plus";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {

        SourcesLocation location = (SourcesLocation) event.getValue();
        if (location == null) {
            ZKUtil.showMessage(ReflectionZKUtil.getLabel("seleccione"), MessageType.WARNING);
        } else {
            String value = location.getDireccion();
            if (value != null && !value.isEmpty()) {
                try {

                    List<Path> list2 = Files.walk(Paths.get(value), Integer.MAX_VALUE).parallel()
                            .filter(path -> !path.toFile().isDirectory())
                            .collect(Collectors.toList());

                    for (int i = 0; i < list2.size(); i++) {
                        int finalI = i;
                        taskExecutor.addProcess(() -> {
                            try {
                                Path e = list2.get(finalI);
                                String mime = Files.probeContentType(e);
                                String name1 = e.getFileName().toString();
                                taskExecutor.setCurrent(name1);
                                if (mime.equalsIgnoreCase("image/gif")
                                        || mime.equalsIgnoreCase("image/jpg")
                                        || mime.equalsIgnoreCase("image/jpeg")
                                        || mime.equalsIgnoreCase("image/png")) {
                                    Image image = new Image();
                                    image.setName(e.getFileName().toString());
                                    String absolutePath = e.toFile().getAbsolutePath();
                                    PathBuilder<?> pathBuilder = crudService.getPathBuilder(Image.class);
                                    StringPath name = pathBuilder.getString("name");
                                    List<?> fetch = crudService.query().from(pathBuilder).where(name.eq(image.name())).fetch();
                                    if (fetch != null && !fetch.isEmpty()) {
                                        image = crudService.refresh((Image) fetch.get(0));

                                    }
                                    if (image.getSize() == null || image.getSize() == 0) {
                                        image.setSize(e.toFile().length());
                                    }

                                    if (image.getSize() == null || image.getSize() == 0) {
                                        image.setSize(e.toFile().length());
                                    }

                                    if (image.getUploadDate() == null)
                                        image.setUploadDate(new Date());

                                    if (image.getTipo() == null) {
                                        switch (mime) {
                                            case "image/gif":
                                                image.setTipo(FileType.GIF);
                                                break;
                                            case "image/jpg":
                                                image.setTipo(FileType.JPG);
                                                break;
                                            case "image/jpeg":
                                                image.setTipo(FileType.JPEG);
                                                break;
                                            default:
                                                image.setTipo(FileType.OTHER);
                                                break;
                                        }
                                    }

                                    if (image.getShorturl() == null || image.getShorturl().isEmpty()) {
                                        image.setShorturl(absolutePath.replace(value, ""));
                                    }
                                    image.setUrl(absolutePath);
                                    crudService.saveOrUpdate(image);
                                }
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        });

                    }
                } catch (Exception w) {
                    w.printStackTrace();
                }
            }
        }

    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(SourcesLocation.class);
    }

    @Override
    public String getLabel() {
        return "Process Image Location";
    }

    @Override
    public String getClasses() {
        return "btn-primary";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 0;
    }

    @Override
    public String getColor() {
        return "#000000";
    }

    @Override
    public int getGroup() {
        return 2;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("Process Image Location");
    }

    @Override
    public boolean isDefault() {
        return true;
    }

    @Override
    public boolean MenuSupported() {
        return true;
    }
}
