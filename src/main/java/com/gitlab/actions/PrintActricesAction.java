package com.gitlab.actions;

import com.gitlab.domain.Actriz;
import com.gitlab.pedrioko.core.lang.Report;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.view.forms.SelectForm;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.util.ZKUtil;
import com.gitlab.pedrioko.services.CrudService;
import com.gitlab.pedrioko.services.ReportService;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.zkoss.zul.Filedownload;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@ToolAction
@Order(0)
public class PrintActricesAction implements Action {

    @Autowired
    private CrudService crudService;
    @Autowired
    private ReportService reportService;

    @Override
    public String getIcon() {
        return "fas fa-print";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {

        SelectForm selectForm = new SelectForm(Report.class);
        selectForm.addAction("Selecionar", "fas fa-plus", "btn btn-primary", e -> {
            Report report = (Report) selectForm.getValue();
            File file = reportService.processReport(report, crudService.getAllOrder(Actriz.class));
            Filedownload.save(new FileInputStream(file), "application/pdf", report.getNombre() + UUID.randomUUID().toString());
            PrinterJob job = PrinterJob.getPrinterJob();
            PrintService printService = null;
            if (job.printDialog()) {
                printService = job.getPrintService();
            } else {
                printService = PrintServiceLookup.lookupDefaultPrintService();
            }
            PDDocument pdDocument = PDDocument.load(file);
            job.setPrintService(printService);
            job.setPageable(new PDFPageable(pdDocument));
            job.print();
            selectForm.detach();
        });
        ZKUtil.showDialogWindow(selectForm);
    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(Actriz.class);
    }

    @Override
    public String getLabel() {
        return "Report";
    }

    @Override
    public String getClasses() {
        return "btn-default";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 0;
    }

    @Override
    public String getColor() {
        return "#aaaaaa";
    }

    @Override
    public int getGroup() {
        return 2;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("Report");
    }

    @Override
    public boolean isDefault() {
        return true;
    }

}
