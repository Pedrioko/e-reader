package com.gitlab.actions;

import com.gitlab.domain.SourcesLocation;
import com.gitlab.domain.Video;
import com.gitlab.domain.enumdomain.FileType;
import com.gitlab.domain.enumdomain.VideoResolution;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.view.enums.MessageType;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.util.ZKUtil;
import com.gitlab.pedrioko.services.CrudService;
import com.gitlab.pedrioko.services.ThreadService;
import com.gitlab.pedrioko.services.VideoService;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@ToolAction
@Order(0)
public class ProcessVideoLocationAction implements Action {

    @Autowired
    private CrudService crudService;
    @Autowired
    private VideoService videoService;
    @Autowired
    private ThreadService taskExecutor;

    @Override
    public String getIcon() {
        return "fas fa-plus";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {

        SourcesLocation location = (SourcesLocation) event.getValue();
        if (location == null) {
            ZKUtil.showMessage(ReflectionZKUtil.getLabel("seleccione"), MessageType.WARNING);
        } else {
            String value = location.getDireccion();
            if (value != null && !value.isEmpty()) {
                try {

                    List<Path> list2 = Files.walk(Paths.get(value), Integer.MAX_VALUE).parallel()
                            .filter(path -> !path.toFile().isDirectory()).filter(e -> {
                                try {
                                    String mime = Files.probeContentType(e);
                                    return mime.equalsIgnoreCase("video/mp4")
                                            || mime.equalsIgnoreCase("video/avi")
                                            || mime.equalsIgnoreCase("video/mov");
                                } catch (IOException ex) {
                                    return false;
                                }

                            }).collect(Collectors.toList());

                    for (int i = 0; i < list2.size(); i++) {
                        int finalI = i;
                        taskExecutor.addProcess(() -> {
                            try {
                                Path e = list2.get(finalI);
                                String mime = Files.probeContentType(e);
                                String name1 = e.getFileName().toString();
                                taskExecutor.setCurrent(name1);
                                Video video = new Video();
                                video.setName(e.getFileName().toString());
                                String absolutePath = e.toFile().getAbsolutePath();
                                PathBuilder<?> pathBuilder = crudService.getPathBuilder(Video.class);
                                StringPath name = pathBuilder.getString("name");
                                try {
                                    List<?> fetch = crudService.query().from(pathBuilder).where(name.eq(video.name())).fetch();
                                    if (fetch != null && fetch.isEmpty()) {
                                        video.setListfiles(videoService.generatePreviewImage(absolutePath));
                                    } else {
                                        video = crudService.refresh((Video) fetch.get(0));
                                        if (video.getListfiles() == null || video.getListfiles().isEmpty() || video.getListfiles().size() < 10)
                                            if (video.getTipo() == null) {
                                                video.addAll(videoService.generatePreviewImage(absolutePath));
                                            } else {
                                                video.addAll(videoService.generatePreviewImage(absolutePath));
                                            }
                                    }
                                } catch (Exception w) {
                                    w.printStackTrace();
                                }
                                if (video.getSize() == null || video.getSize() == 0) {
                                    video.setSize(e.toFile().length());
                                }
                                if (video.getDuration() == null || video.getDuration() == 0) {
                                    video.setDuration(videoService.getTime(absolutePath));
                                }
                                if (video.getResolution() == null) {
                                    Integer resolution = videoService.getResolution(absolutePath);
                                    if (isBetween(resolution, 0, 100))
                                        video.setResolution(VideoResolution.R0);
                                    if (isBetween(resolution, 200, 300))
                                        video.setResolution(VideoResolution.R240);
                                    if (isBetween(resolution, 300, 400))
                                        video.setResolution(VideoResolution.R360);
                                    if (isBetween(resolution, 400, 600))
                                        video.setResolution(VideoResolution.R480);
                                    if (isBetween(resolution, 600, 800))
                                        video.setResolution(VideoResolution.R720);
                                    if (isBetween(resolution, 800, 1100))
                                        video.setResolution(VideoResolution.R1080);
                                    if (isBetween(resolution, 1100, 1500))
                                        video.setResolution(VideoResolution.R1440);
                                    if (isBetween(resolution, 1500, 5000))
                                        video.setResolution(VideoResolution.R4K);
                                }

                                if (video.getSize() == null || video.getSize() == 0) {
                                    video.setSize(e.toFile().length());
                                }

                                if (video.getUploadDate() == null)
                                    video.setUploadDate(new Date());

                                if (video.getTipo() == null) {
                                    switch (mime) {
                                        case "video/mp4":
                                            video.setTipo(FileType.MP4);
                                            break;
                                        case "video/avi":
                                            video.setTipo(FileType.AVI);
                                            break;
                                        case "video/mov":
                                            video.setTipo(FileType.MOV);
                                            break;
                                        default:
                                            video.setTipo(FileType.OTHER);
                                            break;
                                    }
                                }

                                if (video.getShorturl() == null || video.getShorturl().isEmpty()) {
                                    video.setShorturl(absolutePath.replace(value, ""));
                                }
                                video.setUrl(absolutePath);
                                crudService.saveOrUpdate(video);

                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        });

                    }
                } catch (Exception w) {
                    w.printStackTrace();
                }
            }
        }

    }

    private boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(SourcesLocation.class);
    }

    @Override
    public String getLabel() {
        return "Process Video Location";
    }

    @Override
    public String getClasses() {
        return "btn-primary";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 0;
    }

    @Override
    public String getColor() {
        return "#FF0000";
    }

    @Override
    public int getGroup() {
        return 2;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("Process Video Location");
    }

    @Override
    public boolean isDefault() {
        return true;
    }

    @Override
    public boolean MenuSupported() {
        return true;
    }
}
