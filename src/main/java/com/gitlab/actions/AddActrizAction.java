package com.gitlab.actions;

import com.gitlab.domain.Actriz;
import com.gitlab.domain.Video;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.view.enums.MessageType;
import com.gitlab.pedrioko.core.view.forms.AddForm;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.util.ZKUtil;
import com.gitlab.pedrioko.core.view.viewers.crud.CrudView;
import com.gitlab.pedrioko.core.zk.component.chosenbox.ChosenBoxImage;
import com.gitlab.pedrioko.services.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import java.util.*;

@ToolAction
@Order(0)
public class AddActrizAction implements Action {

    @Autowired
    private CrudService crudService;
    private static final String AGREGAR = "Agregar";

    @Override
    public String getIcon() {
        return "fa  fa-female";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {

        Video video = (Video) crudService.refresh(event.getValue());
        if (video == null) {
            ZKUtil.showMessage(ReflectionZKUtil.getLabel("seleccione"), MessageType.WARNING);
        } else {
            AddForm addForm = new AddForm("Actriz", Actriz.class, ChosenBoxImage.class, e -> {
                List<Actriz> actrices = video.getActrices();
                actrices.clear();
                ChosenBoxImage combobox;
                combobox = (ChosenBoxImage) ((AddForm) e.getTarget().getParent().getParent()).getComponentField("Actriz");
                actrices.addAll((Collection) combobox.getValue());
                crudService.saveOrUpdate(video);
                e.getTarget().getParent().getParent().detach();
                ZKUtil.showMessage(ReflectionZKUtil.getLabel("userbasicform.guardar"), MessageType.SUCCESS);
                CrudView crudViewParent = event.getCrudViewParent();
                if (crudViewParent != null) crudViewParent.update();
            });
            addForm.setWidth("500px");
            ZKUtil.showDialogWindow(addForm);
        }
    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(Video.class);
    }

    @Override
    public String getLabel() {
        return "Add actriz";
    }

    @Override
    public String getClasses() {
        return "btn-info";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 0;
    }

    @Override
    public String getColor() {
        return "#0099aa";
    }

    @Override
    public int getGroup() {
        return 2;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("Agregar actriz");
    }

    @Override
    public boolean MenuSupported() {
        return true;
    }

    @Override
    public boolean showLabel() {
        return true;
    }
}
