package com.gitlab.actions;

import com.gitlab.domain.*;
import com.gitlab.pedrioko.core.lang.FileEntity;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.view.enums.MessageType;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.util.ZKUtil;
import com.gitlab.pedrioko.services.CrudService;
import com.gitlab.pedrioko.services.ThreadService;
import com.gitlab.pedrioko.services.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ToolAction
@Order(0)
public class ProcessMangaLocationAction implements Action {

    @Autowired
    private CrudService crudService;
    @Autowired
    private VideoService videoService;
    @Autowired
    private ThreadService taskExecutor;

    @Override
    public String getIcon() {
        return "fas fa-plus";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {

        MangaSourceLocation location = (MangaSourceLocation) event.getValue();
        if (location == null) {
            ZKUtil.showMessage(ReflectionZKUtil.getLabel("seleccione"), MessageType.WARNING);
        } else {
            String value = location.getDireccion();
            if (value != null && !value.isEmpty()) {
                try {
                    List<Path> list2 = Files.walk(Paths.get(location.getDireccion()), Integer.MAX_VALUE).parallel().
                            filter(Files::isDirectory)
                            .collect(Collectors.toList());
                    list2.remove(0);
                    taskExecutor.addProcess(() -> {
                        list2.stream().forEach(dir -> {
                            try {
                                Manga manga = new Manga();
                                manga.setName(dir.getFileName().toString());
                                manga.setUrl(dir.toFile().getAbsolutePath());
                                ArrayList<MangaPage> pages = new ArrayList<>();
                                List<Path> files = Files.walk(Paths.get(dir.toUri()), Integer.MAX_VALUE).parallel()
                                        .filter(path -> !path.toFile().isDirectory())
                                        .collect(Collectors.toList());

                                files.forEach(file -> {
                                    String name1 = file.getFileName().toString();
                                    taskExecutor.setCurrent(name1);
                                    MangaPage page = new MangaPage();
                                    FileEntity pagefile = new FileEntity();
                                    pagefile.setFilename(name1);
                                    pagefile.setUrl(file.toAbsolutePath().toString());
                                    page.setPageFile(pagefile);
                                    pages.add(page);

                                });
                                manga.setPages(pages);
                                crudService.saveOrUpdate(manga);

                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        });
                    });
                } catch (Exception w) {
                    w.printStackTrace();
                }
            }
        }

    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(MangaSourceLocation.class);
    }

    @Override
    public String getLabel() {
        return "Process Image Location";
    }

    @Override
    public String getClasses() {
        return "btn-primary";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 0;
    }

    @Override
    public String getColor() {
        return "#121243";
    }

    @Override
    public int getGroup() {
        return 2;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("Process Location");
    }

    @Override
    public boolean isDefault() {
        return true;
    }

    @Override
    public boolean MenuSupported() {
        return true;
    }
}
