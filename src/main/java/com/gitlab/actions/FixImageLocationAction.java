package com.gitlab.actions;

import com.gitlab.domain.Image;
import com.gitlab.domain.SourcesLocation;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.services.CrudService;
import com.gitlab.pedrioko.services.ThreadService;
import com.gitlab.pedrioko.services.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@ToolAction
@Order(0)
public class FixImageLocationAction implements Action {

    @Autowired
    private CrudService crudService;
    @Autowired
    private VideoService videoService;
    @Autowired
    private ThreadService taskExecutor;

    @Override
    public String getIcon() {
        return "fas fa-minus";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {
        taskExecutor.addProcess(() -> {
            List<Image> all = crudService.getAll(Image.class);
            List<Image> collect = all.stream().filter(e -> !new File(e.getUrl()).exists()).collect(Collectors.toList());
            System.out.println(collect);
            collect.forEach(crudService::delete);
        });

    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(SourcesLocation.class);
    }

    @Override
    public String getLabel() {
        return "Process Fix Image by Location";
    }

    @Override
    public String getClasses() {
        return "btn-primary";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 0;
    }

    @Override
    public String getColor() {
        return "#454545";
    }

    @Override
    public int getGroup() {
        return 3;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("Process Fix Image by Location");
    }

    @Override
    public boolean isDefault() {
        return true;
    }

    @Override
    public boolean MenuSupported() {
        return true;
    }
}
