package com.gitlab.actions;

import com.gitlab.domain.Video;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.services.CrudService;
import com.gitlab.pedrioko.services.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ToolAction
public class FilterVideosEmptiesAction implements Action {

    @Autowired
    private CrudService crudService;
    @Autowired
    private VideoService videoService;

    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    @Override
    public String getIcon() {
        return "fas fa-sort-amount-down";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(null);
        event.getCrudViewParent().addParams("listfiles", arrayList);
        event.getCrudViewParent().getCrudController().doQuery();
    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(Video.class);
    }

    @Override
    public String getLabel() {
        return "";
    }

    @Override
    public String getClasses() {
        return "btn";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 5;
    }

    @Override
    public String getColor() {
        return "#FF00db";
    }

    @Override
    public int getGroup() {
        return 3;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("Filter Empty");
    }

    @Override
    public boolean isDefault() {
        return true;
    }
}
