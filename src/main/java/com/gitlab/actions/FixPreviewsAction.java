package com.gitlab.actions;

import com.gitlab.domain.Video;
import com.gitlab.pedrioko.core.lang.FileEntity;
import com.gitlab.pedrioko.core.lang.annotation.ToolAction;
import com.gitlab.pedrioko.core.view.action.api.Action;
import com.gitlab.pedrioko.core.view.action.event.CrudActionEvent;
import com.gitlab.pedrioko.core.view.enums.FormStates;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.services.CrudService;
import com.gitlab.pedrioko.services.VideoService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@ToolAction
public class FixPreviewsAction implements Action {

    @Autowired
    private CrudService crudService;
    @Autowired
    private VideoService videoService;

    @Override
    public String getIcon() {
        return "fas fa- fa-wrench";
    }

    @Override
    public void actionPerform(CrudActionEvent event) {
        List<Video> all = crudService.getAll(Video.class);
        all.stream().filter(e -> e.getListfiles().size() > 10).forEach(e -> {
            List<FileEntity> listfiles = e.getListfiles();
            listfiles.stream().sorted(Comparator.comparingLong(x -> x.getId()));
            List<FileEntity> fileEntities = listfiles.subList(0, 10);
            e.setListfiles(fileEntities);
            crudService.save(e);
        });
    }

    @Override
    public List<?> getAplicateClass() {
        return Arrays.asList(Video.class);
    }

    @Override
    public String getLabel() {
        return "";
    }

    @Override
    public String getClasses() {
        return "btn-primary";
    }

    @Override
    public FormStates getFormState() {
        return FormStates.READ;
    }

    @Override
    public Integer position() {
        return 3;
    }

    @Override
    public String getColor() {
        return "#FF00FF";
    }

    @Override
    public int getGroup() {
        return 3;
    }

    @Override
    public String getTooltipText() {
        return ReflectionZKUtil.getLabel("Fix previews");
    }

    @Override
    public boolean isDefault() {
        return true;
    }
}
