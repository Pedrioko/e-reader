package com.gitlab.providers;

import com.gitlab.domain.Video;
import com.gitlab.domain.enumdomain.FileType;
import com.gitlab.pedrioko.core.lang.annotation.Menu;
import com.gitlab.pedrioko.core.view.api.MenuProvider;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.viewers.crud.CrudView;
import org.zkoss.zk.ui.Component;

import java.util.LinkedHashMap;
import java.util.Map;

@Menu
public class VideoMenuProvider implements MenuProvider {

    @Override
    public String getLabel() {
        return ReflectionZKUtil.getLabel("Videos");
    }

    @Override
    public Component getView() {
        Map<String, Object> filter= new LinkedHashMap<>();
        filter.put("tipo", FileType.MP4);
        return new CrudView(Video.class,24, filter);
    }

    @Override
    public String getIcon() {
        return "fas fa-video";
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getGroup() {
        return "Media";
    }
}
