package com.gitlab.providers;

import com.gitlab.domain.Video;
import com.gitlab.domain.enumdomain.FileType;
import com.gitlab.pedrioko.core.lang.AppParam;
import com.gitlab.pedrioko.core.lang.annotation.Menu;
import com.gitlab.pedrioko.core.view.api.MenuProvider;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.services.CrudService;
import com.gitlab.pedrioko.services.ThreadService;
import com.gitlab.pedrioko.services.VideoService;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Menu
@Order(0)
public class VideoConfigMenuProvider implements MenuProvider {

    @Autowired
    private CrudService crudService;
    @Autowired
    private VideoService videoService;
    private AppParam p;
    @Autowired
    private ThreadService taskExecutor;

    @Override
    public String getLabel() {
        return ReflectionZKUtil.getLabel("Video Config");
    }

    @Override
    public Component getView() {
        List<AppParam> folder_hdd = crudService.getLikePrecise(AppParam.class, "VIDEO_FOLDER_HDD");
        if (folder_hdd.size() == 0) {
            p = new AppParam();
            p.setName("VIDEO_FOLDER_HDD");
            p.setValue("");
            p = crudService.saveOrUpdate(p);
        } else {
            p = folder_hdd.get(0);
        }
        Window window = new Window();
        Textbox child = new Textbox();
        child.setValue(p.getValue());
        window.appendChild(child);
        Button b = new Button("Save");
        b.addEventListener(Events.ON_CLICK, E -> {
            if (p.getValue() != child.getValue()) {
                p.setValue(child.getValue());
                p = crudService.saveOrUpdate(p);
            }
            String value = p.getValue();
            if (value != null && !value.isEmpty()) {

                List<Path> list2 = Files.walk(Paths.get(value), Integer.MAX_VALUE).parallel()
                        .filter(path -> path.toFile().getAbsolutePath().endsWith(".mp4") || path.toFile().getAbsolutePath().endsWith(".avi") || path.toFile().getAbsolutePath().endsWith(".webm") || path.toFile().getAbsolutePath().endsWith(".gif"))
                        .collect(Collectors.toList());

                for (int i = 0; i < list2.size(); i++) {
                    int finalI = i;
                    taskExecutor.addProcess(() -> {
                        try {
                            Path e = list2.get(finalI);
                            String mime = Files.probeContentType(e);
                            Video video = new Video();
                            video.setName(e.getFileName().toString());
                            String absolutePath = e.toFile().getAbsolutePath();
                            video.setUrl(absolutePath);
                            taskExecutor.setCurrent(video.getName());

                            PathBuilder<?> pathBuilder = crudService.getPathBuilder(Video.class);
                            StringPath name = pathBuilder.getString("name");
                            List<?> fetch = crudService.query().from(pathBuilder).where(name.eq(video.name())).fetch();
                            if (fetch != null && fetch.isEmpty()) {
                                video.setListfiles(videoService.generatePreviewImage(absolutePath));

                            } else {
                                video = (Video) fetch.get(0);
                                if (video.getSize() == null || video.getSize() == 0) {
                                    video.setSize(e.toFile().length());
                                }
                                video.getListfiles().addAll(videoService.generatePreviewImage(absolutePath));
                                if (video.getDuration() == null || video.getDuration() == 0) {
                                    video.setDuration(videoService.getTime(absolutePath));
                                }
                            }
                            if (video.getSize() == null || video.getSize() == 0) {
                                video.setSize(e.toFile().length());
                            }

                            if (video.getUploadDate() == null)
                                video.setUploadDate(new Date());

                            if (video.getDuration() == null || video.getDuration() == 0) {
                                video.setDuration(videoService.getTime(absolutePath));
                            }
                            switch (mime) {
                                case "image/png":
                                    video.setTipo(FileType.PNG);
                                    break;
                                case "video/mp4":
                                    video.setTipo(FileType.MP4);
                                    break;
                                case "video/avi":
                                    video.setTipo(FileType.AVI);
                                    break;
                                case "video/webm":
                                    video.setTipo(FileType.WEBM);
                                    break;
                                case "image/gif":
                                    video.setTipo(FileType.GIF);
                                    break;
                                case "image/jpg":
                                    video.setTipo(FileType.JPG);
                                    break;
                                case "image/jpeg":
                                    video.setTipo(FileType.JPEG);
                                    break;
                                default:
                                    video.setTipo(FileType.OTHER);
                                    break;
                            }
                            crudService.saveOrUpdate(video);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    });

                }
            }
        });
        window.appendChild(b);
        return window;
    }


    @Override
    public String getIcon() {
        return "z-icon-home";
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getGroup() {
        return "Media";
    }

    @Override
    public boolean isOpenByDefault() {
        return false;
    }
}
