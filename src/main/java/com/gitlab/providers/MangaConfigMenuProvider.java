package com.gitlab.providers;

import com.gitlab.domain.Manga;
import com.gitlab.pedrioko.core.lang.AppParam;
import com.gitlab.pedrioko.core.lang.FileEntity;
import com.gitlab.pedrioko.core.lang.annotation.Menu;
import com.gitlab.pedrioko.core.view.api.MenuProvider;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.services.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Menu
@Order(0)
public class MangaConfigMenuProvider implements MenuProvider {

    @Autowired
    private CrudService crudService;
    private AppParam p;

    @Override
    public String getLabel() {
        return ReflectionZKUtil.getLabel("Manga Config");
    }

    @Override
    public Component getView() {
        List<AppParam> folder_hdd = crudService.getLikePrecise(AppParam.class, "FOLDER_HDD");
        if (folder_hdd.size() == 0) {
            p = new AppParam();
            p.setName("FOLDER_HDD");
            p.setValue("");
            p = crudService.saveOrUpdate(p);
        } else {
            p = folder_hdd.get(0);
        }
        Window window = new Window();
        Textbox child = new Textbox();
        child.setValue(p.getValue());
        window.appendChild(child);
        Button b = new Button("Save");
        b.addEventListener(Events.ON_CLICK, E -> {
            if (p.getValue() != child.getValue()) {
                p.setValue(child.getValue());
                p = crudService.saveOrUpdate(p);
            }
            List<Path> list2 = Files.walk(Paths.get(p.getValue()), Integer.MAX_VALUE).parallel().
                    filter(Files::isDirectory)
                    .collect(Collectors.toList());
            list2.remove(0);
            list2.parallelStream().forEach(e -> {
                        Manga manga = new Manga();
                        manga.setName(e.getFileName().toString());
                        manga.setUrl(e.toFile().getAbsolutePath());
                        try {
                            Optional<Path> first = Files.walk(Paths.get(manga.getUrl()), Integer.MAX_VALUE).
                                    filter(Files::isRegularFile).findFirst();
                            if (first.isPresent()) {
                                List<Manga> m = crudService.getLikePrecise(Manga.class, manga.getName());
                                if (m.size() == 0) {
                                    FileEntity imageurl = new FileEntity();
                                    File path = first.get().toFile();
                                    imageurl.setUrl(path.getAbsolutePath());
                                    imageurl.setFilename(path.getName());
                                    manga.setImageurl(imageurl);
                                    crudService.saveOrUpdate(manga);
                                }
                            }
                        } catch (IOException e1) {
                        }
                    }
            );
        });
        window.appendChild(b);
        return window;
    }


    @Override
    public String getIcon() {
        return "z-icon-home";
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getGroup() {
        return "Mangas";
    }

    @Override
    public boolean isOpenByDefault() {
        return false;
    }
}
