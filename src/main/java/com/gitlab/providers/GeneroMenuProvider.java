package com.gitlab.providers;

import com.gitlab.domain.Genero;
import com.gitlab.pedrioko.core.lang.annotation.Menu;
import com.gitlab.pedrioko.core.view.api.MenuProvider;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.viewers.crud.CrudView;
import org.springframework.core.annotation.Order;
import org.zkoss.zk.ui.Component;

@Menu
@Order(0)
public class GeneroMenuProvider implements MenuProvider {

    @Override
    public String getLabel() {
        return ReflectionZKUtil.getLabel("Genero");
    }

    @Override
    public Component getView() {
        CrudView crudView = new CrudView(Genero.class);
        crudView.setPageSize(24);
        return crudView;
    }

    @Override
    public String getIcon() {
        return "fas fa-genderless";
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getGroup() {
        return "Media";
    }

}
