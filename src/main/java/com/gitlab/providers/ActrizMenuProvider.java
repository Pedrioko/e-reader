package com.gitlab.providers;

import com.gitlab.domain.Actriz;
import com.gitlab.pedrioko.core.lang.annotation.Menu;
import com.gitlab.pedrioko.core.view.api.MenuProvider;
import com.gitlab.pedrioko.core.reflection.ReflectionZKUtil;
import com.gitlab.pedrioko.core.view.viewers.crud.CrudView;
import org.springframework.core.annotation.Order;
import org.zkoss.zk.ui.Component;

@Menu
@Order(0)
public class ActrizMenuProvider implements MenuProvider {

    @Override
    public String getLabel() {
        return ReflectionZKUtil.getLabel("Actriz");
    }

    @Override
    public Component getView() {
        CrudView crudView = new CrudView(Actriz.class,24);
        crudView.useAlphabetFilter();
        return crudView;
    }

    @Override
    public String getIcon() {
        return "fas fa-female";
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getGroup() {
        return "Media";
    }

}
