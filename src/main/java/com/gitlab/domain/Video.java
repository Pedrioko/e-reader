package com.gitlab.domain;

import com.gitlab.domain.enumdomain.FileType;
import com.gitlab.domain.enumdomain.VideoResolution;
import com.gitlab.pedrioko.core.lang.FileEntity;
import com.gitlab.pedrioko.core.lang.annotation.CrudOrderBy;
import com.gitlab.pedrioko.core.lang.annotation.Duration;
import com.gitlab.pedrioko.core.lang.annotation.FileSize;
import com.gitlab.pedrioko.core.lang.annotation.UseChosenFileEntity;
import com.gitlab.pedrioko.core.lang.BaseEntity;

import javax.persistence.*;
import java.util.*;

@Entity

@CrudOrderBy(value = "name")
public
class Video extends BaseEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(nullable = false)
    private long id;
    @Lob
    @Column(length = 50000)
    private String url;
    @Lob
    @Column(length = 50000)
    private String imageurl;
    @Lob
    @Column(length = 50000)
    private String name;
    @Lob
    @Column(length = 50000)
    private String visualname;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "video_categoria",
            joinColumns = @JoinColumn(name = "video_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "categoria_id", referencedColumnName = "id"))
    @UseChosenFileEntity(orderBy = "name")
    private Set<Categoria> categorias;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "video_actrices",
            joinColumns = @JoinColumn(name = "video_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "actriz_id", referencedColumnName = "id"))
    @UseChosenFileEntity(orderBy = "nombre")
    private Set<Actriz> actrices;
    @Lob
    private String shorturl;
    @OneToMany(cascade = CascadeType.ALL)
    private Set<FileEntity> listfiles;
    @Enumerated(EnumType.STRING)
    private FileType tipo;
    @Enumerated(EnumType.STRING)
    private VideoResolution resolution;
    @FileSize
    private Long size;

    private Date uploadDate = new Date();
    @Column
    @Duration
    private Double duration;

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String name() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String getVisualname() {
        return visualname;
    }

    public void setVisualname(String visualname) {
        this.visualname = visualname;
    }

    public List<Categoria> getCategorias() {
        return new ArrayList<>(categorias);
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = new LinkedHashSet<>(categorias);
    }

    public List<Actriz> getActrices() {
        return new ArrayList<>(actrices);
    }

    public void setActrices(List<Actriz> actrices) {
        this.actrices = new LinkedHashSet<>(actrices);
    }

    public List<FileEntity> getListfiles() {
        return new ArrayList<>(listfiles);
    }

    public void setListfiles(List<FileEntity> listfiles) {
        this.listfiles = new LinkedHashSet<>(listfiles);
    }

    public FileType getTipo() {
        return tipo;
    }

    public void setTipo(FileType tipo) {
        this.tipo = tipo;
    }

    public VideoResolution getResolution() {
        return resolution;
    }

    public void setResolution(VideoResolution resolution) {
        this.resolution = resolution;
    }

    public String getShorturl() {
        return shorturl;
    }

    public void setShorturl(String shorturl) {
        this.shorturl = shorturl;
    }

    public String getName() {
        return name;
    }

    public boolean addAll(Collection<? extends FileEntity> c) {
        return listfiles.addAll(c);
    }
}
