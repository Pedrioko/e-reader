package com.gitlab.domain.enumdomain;

public enum VideoResolution {
    R0,R240, R360, R480, R720, R1080, R1440, R4K
}
