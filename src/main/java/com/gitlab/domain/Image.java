package com.gitlab.domain;

import com.gitlab.domain.enumdomain.FileType;
import com.gitlab.pedrioko.core.lang.annotation.*;
import com.gitlab.pedrioko.core.lang.BaseEntity;

import javax.persistence.*;
import java.util.*;

@Entity

@CrudOrderBy(value = "name")
public
class Image extends BaseEntity{


    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(nullable = false)
    private long id;
    @Lob
    @Column(length = 50000)
    private String url;
    @Lob
    @Column(length = 50000)
    private String imageurl;
    @Lob
    @Column(length = 50000)
    private String name;
    @Lob
    @Column(length = 50000)
    private String visualname;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "image_categoria",
            joinColumns = @JoinColumn(name = "image_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "categoria_id", referencedColumnName = "id"))
    @NoEmpty
    @UseChosenFileEntity(orderBy = "name")
    private List<Categoria> categorias;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "image_actrices",
            joinColumns = @JoinColumn(name = "image_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "actriz_id", referencedColumnName = "id"))
    @NoEmpty
    @UseChosenFileEntity(orderBy = "nombre")
    private List<Actriz> actrices;
    @Lob
    private String shorturl;
    @Enumerated(EnumType.STRING)
    private FileType tipo;

    @FileSize
    private Long size;

    private Date uploadDate = new Date();

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String name() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String getVisualname() {
        return visualname;
    }

    public void setVisualname(String visualname) {
        this.visualname = visualname;
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }

    public List<Actriz> getActrices() {
        return actrices;
    }

    public void setActrices(List<Actriz> actrices) {
        this.actrices = actrices;
    }

    public FileType getTipo() {
        return tipo;
    }

    public void setTipo(FileType tipo) {
        this.tipo = tipo;
    }

    public String getShorturl() {
        return shorturl;
    }

    public void setShorturl(String shorturl) {
        this.shorturl = shorturl;
    }

    public String getName() {
        return name;
    }

}
