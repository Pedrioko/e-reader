package com.gitlab.domain;

import com.gitlab.pedrioko.core.lang.annotation.*;
import com.gitlab.pedrioko.core.lang.BaseEntity;

import javax.persistence.*;

@Entity

@CrudOrderBy(value = "nombre")
public
class SourcesLocation extends BaseEntity{


    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(nullable = false)
    private long id;
    @Lob
    @Column(length = 50000)
    private String nombre;
    @Lob
    @Column(length = 50000)
    private String direccion;


    public SourcesLocation() {
    }

    public SourcesLocation(String nombre, String direccion) {
        this.nombre = nombre;
        this.direccion = direccion;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }


    @Override
    public String toString() {
        return "SourcesLocation{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                '}';
    }
}
