package com.gitlab.domain;

import com.gitlab.pedrioko.core.lang.FileEntity;
import com.gitlab.pedrioko.core.lang.annotation.NoEmpty;
import com.gitlab.pedrioko.core.lang.BaseEntity;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.zkoss.image.AImage;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity

public
class Manga extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(nullable = false)
    private long id;
    @Lob
    @Column(length = 50000)
    private String url;
    @Lob
    @Column(length = 50000)
    private FileEntity imageurl;
    @Lob
    @Column(length = 50000)
    private String name;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "manga_categoria",
            joinColumns = @JoinColumn(name = "manga_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "categoria_id", referencedColumnName = "id"))
    @NoEmpty
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Categoria> categorias;


    private transient AImage preview;
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(
            name = "manga_pages",
            joinColumns = @JoinColumn(name = "manga_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "page_id", referencedColumnName = "id"))
    @OneToMany(cascade = CascadeType.ALL)
    private List<MangaPage> pages;

    public Manga() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public FileEntity getImageurl() {
        return imageurl;
    }

    public void setImageurl(FileEntity imageurl) {
        this.imageurl = imageurl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }

    public AImage getPreview() {
        return preview;
    }

    public void setPreview(AImage preview) {
        this.preview = preview;
    }

    public List<MangaPage> getPages() {
        return pages;
    }

    public void setPages(List<MangaPage> pages) {
        this.pages = pages;
    }
}
