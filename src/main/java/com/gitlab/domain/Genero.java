package com.gitlab.domain;


import com.gitlab.pedrioko.core.lang.FileEntity;
import com.gitlab.pedrioko.core.lang.annotation.*;
import com.gitlab.pedrioko.core.view.api.ChosenItem;
import com.gitlab.pedrioko.core.lang.BaseEntity;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;

@Entity
@CrudOrderBy(value = "nombre")
@NoDuplicate(value = "nombre")
@AlphabetSearch(field = "nombre")
public class Genero extends BaseEntity implements ChosenItem {


    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(nullable = false)
    private long id;
    @CapitalizeFully
    private String nombre;

    @ImageFileEntity
    @OneToOne(cascade = CascadeType.ALL)
    private FileEntity icono;

    public Genero() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public FileEntity getIcono() {
        return icono;
    }

    public void setIcono(FileEntity icono) {
        this.icono = icono;
    }

    @Override
    public String toString() {
        return nombre;
    }

    @Override
    public List<FileEntity> filesEntities() {
        return Arrays.asList(icono);
    }

    @Override
    public String visualName() {
        return getNombre();
    }
}
