package com;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * The Class com.ereader.Application.
 */
//@SpringBootApplication
//@Configuration
//@EntityScan(basePackages = {"com.gitlab"})
//@PropertySource("classpath:/applicationasian.properties")
public class AppAsian {

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(AppAsian.class);
        ConfigurableApplicationContext applicationContext = builder.headless(false).run(args);

    }

}
